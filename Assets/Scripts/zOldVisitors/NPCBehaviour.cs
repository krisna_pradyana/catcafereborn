﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using Pathfinding;

public class NPCBehaviour : MonoBehaviour, IPoolable
{
    public enum GraphMaskAI
    {
        None        = 0,
        Customer    = 1 << 0,
        Pedestrian  = 1 << 1
    }

    public event System.Action<NPCBehaviour> OnSpawned;
    public event System.Action<NPCBehaviour> OnDespawned;

    public enum NPCCharacteristic { kid, adult, schoolar, gamer, officer }
    public enum NPCType { pedestrian, customer }

    public enum LastDirection { north, east , south, west}

    public NPCCharacteristic npcCharacteristic;
    public NPCType npcType;
    public LastDirection lastDirection = LastDirection.south;

    public bool isCustomer;

    public bool trayTest;

    Animator npcAnimator;

    SpriteRenderer npcSprite;
    VisitorAI aiPath;
    Seeker seeker;

    VisitorPathfinding pathfinding;
    Customer customer;

    Vector3 vectorX, vectorZ;

    private void Awake()
    {
        npcAnimator = transform.GetChild(0).GetComponent<Animator>();
        npcSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();

        aiPath = GetComponent<VisitorAI>();
        seeker = GetComponent<Seeker>();

        pathfinding = GetComponent<VisitorPathfinding>();
        customer = GetComponent<Customer>();
    }

    void IPoolable.OnSpawned()
    {
        npcCharacteristic = (NPCCharacteristic)Random.Range(0, 5);
        isCustomer = Random.value > .5f;

        if (isCustomer)
        {
            seeker.graphMask = (int)GraphMaskAI.Customer;
            customer.StartBehaviour();
        }
        else
        {
            seeker.graphMask = (int)GraphMaskAI.Pedestrian;
            pathfinding.LeaveGameArea();
        }

        if (OnSpawned != null)
            OnSpawned(this);
    }

    void IPoolable.OnDespawned()
    {
        if (OnDespawned != null)
            OnDespawned(this);
    }

    private void Update()
    {
        //if (pathfinding.leavingGameArea && pathfinding.HasArrivedAtDestination())
        //npcAnimator.SetBool("isTray", trayTest);
        //Debug.Log(aiPath.velocity.x);

        if (aiPath.reachedDestination)
        {
            ObjectPooler.Despawn(gameObject);
        }

        SetAnimation();
    }

    public void RandomizeNPC()
    {
        npcCharacteristic = (NPCCharacteristic)Random.Range(0, 2);
        npcType = (NPCType)Random.Range(0, 5);
    }

    private void SetAnimation()
    {
        Vector3 velocity = aiPath.velocity;

        npcAnimator.SetFloat("Xaxis", velocity.x);
        npcAnimator.SetFloat("Zaxis", velocity.z);

        if(Mathf.Abs(velocity.z) > 0 || Mathf.Abs(velocity.x) > 0)
        {
            Debug.Log("Ai is moving");
            npcAnimator.SetBool("isIdle", false);
        }
        else
        {
            Debug.Log("Ai is idling");
            npcAnimator.SetBool("isIdle", true);
        }

        if (Mathf.Abs(velocity.z) > Mathf.Abs(velocity.x))
            npcSprite.flipX = true;

        else if (Mathf.Abs(velocity.z) < Mathf.Abs(velocity.x))
            npcSprite.flipX = false;

        SetLastDirection(velocity.x, velocity.z);
    }

    /// <summary>
    /// Set default rotaton based on orthographic render camera
    /// </summary>
    private void SetLookAtCamera()
    {
        npcSprite.transform.rotation = Camera.main.transform.rotation;
    }

    /// <summary>
    /// this determine the way NPC Idle, based on last NPC movement direction
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name=""></param>
    void SetLastDirection(float x, float z)
    {
        if (x == 0 && z == 0) return;

        if (Mathf.Abs(z) > Mathf.Abs(x))
        {
            if (z > 0) lastDirection = LastDirection.north; 
            else lastDirection = LastDirection.south;
        }
        else
        {
            if (x > 0) lastDirection = LastDirection.east;
            else lastDirection = LastDirection.west;
        }

        npcAnimator.SetBool("isPositive", lastDirection == LastDirection.north || lastDirection == LastDirection.east);
    }
}
