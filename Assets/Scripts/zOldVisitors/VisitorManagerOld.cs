﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorManagerOld : MonoBehaviour
{
    public static VisitorManagerOld instance;

    public GameObject visitorPrefabs;

    [Space]
    public Transform[] visitorSpawnPoints;
    
    public Transform[] visitorExits { get { return visitorSpawnPoints; } }

    private List<NPCBehaviour> npcBehaviours;

    private void Awake()
    {
        #region Singleton
        if (instance != null)
        {
            Debug.LogErrorFormat(instance, "There cannot be more than 1 VisitorManager.");
            Destroy(this);
        }

        else instance = this;
        #endregion

        ObjectPooler.CreatePool(visitorPrefabs);
    }

    private void Start()
    {
        for (int i = 0; i < visitorSpawnPoints.Length; i++)
            StartCoroutine(SpawnCoroutine(i));

        //while (true)
        //{
        //    yield return new WaitForSeconds(10f);

        //    int spawnIndex = Random.Range(0, visitorSpawnPoints.Length);
        //    GameObject obj = ObjectPooler.Spawn(visitorPrefabs, visitorSpawnPoints[spawnIndex].position, Quaternion.identity);
        //}
    }
    
    private IEnumerator SpawnCoroutine(int spawnIndex)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 10f));
            GameObject obj = ObjectPooler.Spawn(visitorPrefabs, visitorSpawnPoints[spawnIndex].position, Quaternion.identity);
        }
    }
}
