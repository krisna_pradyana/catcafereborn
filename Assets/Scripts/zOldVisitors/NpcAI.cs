﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcAI : Pathfinding.AIPath
{
    public float avoidanceRadius = 1f;
    public float avoidanceWeight = 0.5f;

    private VisitorManager visitorManager;

    protected override void Start()
    {
        base.Start();
        visitorManager = VisitorManager.instance;
    }

    private void GetAvoidanceVelocity()
    {
        
    }

    protected override void MovementUpdateInternal(float deltaTime, out Vector3 nextPosition, out Quaternion nextRotation)
    {
        base.MovementUpdateInternal(deltaTime, out nextPosition, out nextRotation);


    }
}
