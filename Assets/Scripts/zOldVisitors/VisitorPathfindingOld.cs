﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorPathfindingOld : MonoBehaviour
{
    public bool leavingGameArea = false;

    public AIPath aiPath;
    public Seeker seeker;

    private VisitorManager visitorManager;

    private void Awake()
    {
        aiPath = GetComponent<AIPath>();
    }

    private void Start()
    {
        visitorManager = VisitorManager.instance;
    }

    public void ResetData()
    {
        leavingGameArea = false;
    }

    public void ForceSearchPath()
    {
        aiPath.SearchPath();
    }

    public bool HasArrivedAtDestination()
    {
        return aiPath.remainingDistance < aiPath.endReachedDistance;
    }

    public void SetDestination(Vector3 destination)
    {
        leavingGameArea = false;
        aiPath.destination = destination;
    }

    public void LeaveGameArea()
    {
        if (visitorManager == null)
            visitorManager = VisitorManager.instance;

        Transform[] visitorExits = visitorManager.visitorExits;
        int exitIndex = Random.Range(0, visitorExits.Length);

        if (transform.position == visitorExits[exitIndex].position)
            exitIndex = (exitIndex + 1) % visitorExits.Length;

        aiPath.destination = visitorExits[exitIndex].position;
        aiPath.SearchPath();

        leavingGameArea = true;
    }
}
