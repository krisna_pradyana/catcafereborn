﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CafeInternalEconomy : MonoBehaviour
{
    public static CafeInternalEconomy instance;

    public System.Action<int> AddMoney { get; set; }
    public System.Action<int> ReduceMoney { get; set; }
    public System.Action DayResult { get; set; }

    public static int currentMoney { get; private set; } //is currently collected money on that day
    public static int collectedMoney { get; private set; } //is all money collected and money that can be used to purchase and upgrade

    public Text currentMoneyText;
    public Text collectedMoneyText;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

        AddMoney += IncreaseMoney;
        ReduceMoney += DecreaseMoney;
        DayResult += ResultMoney;
    }

    private void Update()
    {
        currentMoneyText.text = currentMoney.ToString();
        collectedMoneyText.text = collectedMoney.ToString();
    }

    private void IncreaseMoney(int amount)
    {
        currentMoney += amount;
    }

    private void DecreaseMoney(int amount)
    {
        currentMoney -= amount;
    }

    private void ResultMoney()
    {
        collectedMoney += currentMoney;
    }
}
