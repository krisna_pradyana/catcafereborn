﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CafeManager : MonoBehaviour
{
    public event System.Action OnQueueUpdate;
    public event System.Action OnDayDone;

    private Queue<Customer> customerQueue = new Queue<Customer>();
    private List<Chair> chairs = new List<Chair>();

    [SerializeField] Transform queueTransform = null;

    public Slider dayTimeSlide;
    public int queueCount = 0;
    public float dayTime;
    [Range(0, 4)] public int maxQueue = 4;
    [Range(0, 3)] public float queueDistance = 1;

    public bool isLongQueue { get { return occupiedChair + customerQueue.Count >= chairs.Count || customerQueue.Count >= maxQueue || occupiedChair == chairs.Count;} }
    public bool isFull { get { return occupiedChair == chairs.Count; } }
    [SerializeField] private int occupiedChair = 0;
    public bool checkIsFull;

    #region Singleton
    public static CafeManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogErrorFormat(instance, "There cannot be more than 1 CafeManager.");
            Destroy(this);
            return;
        }

        instance = this;
        OnDayDone += () => { VisitorManager.instance.cafePower = 0; };
    }
    #endregion

    private void Start()
    {
        StartCoroutine(GameTime());
    }

    private void Update()
    {
        if (dayTimeSlide.value < dayTime)
        {
            dayTimeSlide.value +=  1 / dayTime * Time.deltaTime;
        }

        checkIsFull = isFull;
        queueCount = customerQueue.Count;
    }

    #region Temp Queue Manager
    public void StartQueue()
    {
        StartCoroutine(QueueTimer());
    }

    private IEnumerator QueueTimer()
    {
        yield return new WaitForSeconds(5f);

        foreach (Customer customer in customerQueue)
            customer.queueNumber--;

        customerQueue.Dequeue();

        if (OnQueueUpdate != null)
            OnQueueUpdate();

        if (customerQueue.Count > 0)
            StartCoroutine(QueueTimer());
    }

    /// <summary>
    /// Get the queue number when a customer arrives
    /// </summary>
    /// <param name="customer">The customer who arrives</param>
    /// <returns>The queue number(zero based) of the customer, -1 if the queue is full</returns>
    public int GetQueueNumber(Customer customer)
    {
        if (customerQueue.Count >= maxQueue)
            return -1;

        if (customerQueue.Count <= 0)
            StartQueue();

        customerQueue.Enqueue(customer);
        return customerQueue.Count - 1;
    }

    public Vector3 GetQueuePosition(int queueNumber)
    {
        return queueTransform.position + (queueTransform.forward * queueNumber * queueDistance);
    }

    public Vector3 GetQueuePosition()
    {
        return queueTransform.position + (queueTransform.forward * customerQueue.Count * queueDistance);
    }
    #endregion

    public void RegisterChair(Chair chair)
    {
        chairs.Add(chair);
        chair.OnMoneyTaken += OnMoneyTaken;
    }

    IEnumerator GameTime()
    {
        yield return new WaitForSeconds(dayTime);
        OnDayDone();

        while (occupiedChair == 0)
            yield return null;

        yield return new WaitForSeconds(5f);
        Debug.Log("Day Done"); // Should Cut Gameplay
        yield break;
    }

    public Chair GetChair(Customer customer)
    {
        Chair chair = chairs.Find((c) => c.isOccupied == false);
        
        if (chair != null)
        {
            chair.isOccupied = true;
            occupiedChair++;
        }

        else Debug.LogErrorFormat(customer, "Customer can get in queue even though the cafe is full.");
        return chair;
    }

    public void OnMoneyTaken()
    {
        occupiedChair--;
    }
}
