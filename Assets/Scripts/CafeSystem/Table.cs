﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chair : MonoBehaviour
{
    public event System.Action OnMoneyTaken; 
    public event System.Action OnCustomnerEat;

    public bool isOccupied = false;
    public bool isHeadPositive = false;

    private SpriteRenderer foodSprite;
    private Button cashButton;
    private GameObject cashCanvas;
    private CafeInternalEconomy internalEconomy;

    /// <summary>
    /// Initiate this funct while there is customer on this table
    /// </summary>
    /// <param name="_sprite"></param>
    public void FoodOnTable(Sprite _sprite)
    {
        foodSprite.sprite = _sprite;
        foodSprite.enabled = true;
    }

    public void LeaveChair()
    {
       // if (OnCustomerLeave != null)
            //OnCustomerLeave();
            foodSprite.enabled = false;
            cashCanvas.GetComponent<Canvas>().enabled = true;
    }

    public void MoneyTaken()
    {
        Debug.Log("Money Taken");
        if (OnMoneyTaken != null)
            OnMoneyTaken();
        isOccupied = false;
        cashCanvas.GetComponent<Canvas>().enabled = false;
        internalEconomy.AddMoney(100);
    }

    private void Start()
    {
        internalEconomy = CafeInternalEconomy.instance;
        CafeManager.instance.RegisterChair(this);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.tag == "Cash")
            {
                cashCanvas = transform.GetChild(i).gameObject;
                cashButton = cashCanvas.transform.GetChild(0).GetComponent<Button>();
            }
        }

        cashCanvas.GetComponent<Canvas>().enabled = false; // disable canvas visibility on start
        cashButton.onClick.AddListener(delegate { MoneyTaken(); }); //adding func after taking cash

        foodSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
        foodSprite.enabled = false; //Set food disabled on start
    }
}

public class Table : MonoBehaviour
{
    public bool isFull { get { return !chairs.Exists((c) => c.isOccupied == false); } }

    private List<Chair> chairs = new List<Chair>();

    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.tag == "CafeChair")
            {
                Chair c = transform.GetChild(i).gameObject.AddComponent<Chair>();
                chairs.Add(c);

                if (c.transform.position.x < transform.position.x)
                    c.isHeadPositive = true;

            }
        }

        
    }

    public Chair GetChair()
    {
        return chairs.Find((c) => c.isOccupied == false);
    }
}
