﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    void OnSpawned();
    void OnDespawned();
}

public class PoolMaker : MonoBehaviour
{
    public GameObject prefabs;
    public int size = 30;

    public bool preload = true;

    private void Awake()
    {
        ObjectPooler.CreatePool(prefabs, size, preload);
    }
}

public static class ObjectPooler
{
    private class Pool
    {
        public Transform objectHolder;
        public GameObject prefabs;
        public int size = 50;

        private Queue<GameObject> instances = new Queue<GameObject>();
        private Stack<GameObject> inactives = new Stack<GameObject>();

        private GameObject mb = new GameObject();
        
        public GameObject Spawn(Vector3 position, Quaternion rotation)
        {
            GameObject obj;

            // If the queue is not full, just instantiate a new object(if the pool is preloaded, this code shouldn't run)
            if (instances.Count < size)
            {
                obj = Object.Instantiate(prefabs);
                obj.AddComponent<PoolMemberData>().pool = this;

                if (objectHolder != null) obj.transform.SetParent(objectHolder);
            }

            // else, if there are inactive instance in the pool, then pick it
            else if (inactives.Count > 0) obj = inactives.Pop();

            // else, just grab the oldest item in the queue
            else obj = instances.Dequeue();

            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.SetActive(true);

            obj.GetComponent<MonoBehaviour>().StartCoroutine(Start(obj));

            instances.Enqueue(obj);
            return obj;
        }

        private IEnumerator Start(GameObject obj)
        {
            yield return null;

            foreach (IPoolable member in obj.GetComponents<IPoolable>())
                member.OnSpawned();
        }

        public void Despawn(GameObject instance)
        {
            if (!instances.Contains(instance))
            {
                Debug.LogErrorFormat(instance, "Trying to despawn an instance that's not from this pool.");
                return;
            }

            foreach (IPoolable member in instance.GetComponents<IPoolable>())
                member.OnDespawned();

            instance.SetActive(false);
            inactives.Push(instance);
        }

        public void Preload()
        {
            for (int i = 0; i < size; i++)
            {
                GameObject obj = Object.Instantiate(prefabs);
                obj.AddComponent<PoolMemberData>().pool = this;
                
                if (objectHolder != null) obj.transform.SetParent(objectHolder);
                obj.SetActive(false);

                instances.Enqueue(obj);
                inactives.Push(obj);
            }
        }
    }

    /// <summary> This class is used to store data on the object spawned by the pool </summary>
    private class PoolMemberData : MonoBehaviour { public Pool pool; }

    private static Dictionary<GameObject, Pool> pools = new Dictionary<GameObject, Pool>();

    public static GameObject Spawn(GameObject prefabs, Vector3 position, Quaternion rotation, bool makePoolIfNotExist = false)
    {
        if (!pools.ContainsKey(prefabs))
        {
            if (makePoolIfNotExist)
                CreatePool(prefabs);
            else
                return null;
        }

        return pools[prefabs].Spawn(position, rotation);
    }

    public static void Despawn(GameObject instance)
    {
        PoolMemberData pmd = instance.GetComponent<PoolMemberData>();

        if (pmd == null)
        {
            Debug.LogErrorFormat(instance, "{0} is not an instance of or spawned by any pool!", instance);
            return;
        }

        pmd.pool.Despawn(instance);
    }

    public static void CreatePool(GameObject prefabs, int size = 50, bool preloadPool = true, bool createHolder = true)
    {
        Pool pool = new Pool();
        pool.prefabs = prefabs;
        pool.size = size;

        pool.objectHolder = new GameObject(prefabs.name + "-Holder").transform;

        if (preloadPool)
            pool.Preload();

        pools.Add(prefabs, pool);
    }
}
