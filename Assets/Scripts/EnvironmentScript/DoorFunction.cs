﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorFunction : MonoBehaviour
{
    Animator doorAnimator;
    bool isPersonPass;

    private void Start()
    {
        doorAnimator = transform.GetChild(0).GetComponent<Animator>();
    }

    private void Update()
    {
        float close = doorAnimator.GetFloat("Open");
        if (!isPersonPass)
        {
            if (doorAnimator.GetFloat("Open") > 0)
            {
                doorAnimator.SetFloat("Open", close -= 0.05f);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        float open = doorAnimator.GetFloat("Open");
        Visitor visitor = other.gameObject.GetComponent<Visitor>();

        if (other.gameObject.CompareTag("Visitors") && visitor.isCustomer)
        {
            isPersonPass = true;
            //doorAnimator.Play("door_Open");
            Debug.Log("Someone opening door");
            if (doorAnimator.GetFloat("Open") < 1)
            {
                doorAnimator.SetFloat("Open", open += 0.1f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Visitor visitor = other.gameObject.GetComponent<Visitor>();

        if (other.gameObject.CompareTag("Visitors") && visitor.isCustomer)
        {
            //doorAnimator.Play("door_Close");
            Debug.Log("Someone closing door");
            isPersonPass = false;
        }
    }
}
