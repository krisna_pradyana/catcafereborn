﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectClickFeatures : MonoBehaviour
{
    public event System.Action OnDesignatedObject;

    Ray ray;
    RaycastHit raycastHit;

    private void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out raycastHit))
        {
            if (Input.GetMouseButtonDown(0))
                CheckClickedObject(raycastHit);
                print(raycastHit.collider.name);
        }
    }

    void CheckClickedObject(RaycastHit hit)
    {
        if(hit.collider.gameObject.tag == "Visitors")
        {
            OnDesignatedObject();
        }
    }
}
