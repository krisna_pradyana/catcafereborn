﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera camera;
    [SerializeField] private Vector3 touchStart;
    [SerializeField] private int verticalMultiplier = 0;
    [SerializeField] private float zoomOutMin = 1;
    [SerializeField] private float zoomOutMax = 8;
    [SerializeField] private GameObject cameraPivot;

    [Header("Bounds")]
    [SerializeField] Vector3 center;
    [SerializeField] RectOffset padding;

    //private float width;

    private Vector3 lastTouchPos;

    public Vector3 topLeftBound { get { return center + transform.up * padding.top + transform.right * -padding.left; } }
    public Vector3 topRightBound { get { return center + transform.up * padding.top + transform.right * padding.right; } }
    public Vector3 bottomLeftBound { get { return center + transform.up * -padding.bottom + transform.right * -padding.left; } }
    public Vector3 bottomRightBound { get { return center + transform.up * -padding.bottom + transform.right * padding.right; } }

    public Vector3 upBoundAxis;
    public Vector3 rightBoundAxis;

    private void Awake()
    {
        upBoundAxis = (topLeftBound - bottomLeftBound).normalized;
        rightBoundAxis = (bottomRightBound - bottomLeftBound).normalized;
    }

    private void Start()
    {
        camera = this.GetComponent<Camera>();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        
        Gizmos.DrawLine(topLeftBound, topRightBound);
        Gizmos.DrawLine(topRightBound, bottomRightBound);
        Gizmos.DrawLine(bottomRightBound, bottomLeftBound);
        Gizmos.DrawLine(bottomLeftBound, topLeftBound);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        ZoomControl();
        ClampToBounds();

    }

    private void FixedUpdate()
    {
        //transform.position = new Vector3(transform.position.x, 19.4f, transform.position.z); //Lock Y transform
    }

    private void ClampToBounds()
    {
        float height = camera.orthographicSize;
        float width  = camera.orthographicSize * camera.aspect;
        float boundWidth = padding.left + padding.right;
        float boundHeight = padding.bottom + padding.top;

        if (width * 2f > boundWidth) camera.orthographicSize = boundWidth / camera.aspect / 2f;
        if (height * 2f > boundHeight) camera.orthographicSize = boundHeight / 2f;

        Vector3 camTopLeft = transform.position + transform.right * -width + transform.up * height - bottomLeftBound;
        Vector3 camTopRight = transform.position + transform.right * width + transform.up * height - bottomLeftBound;
        Vector3 camBottomLeft = transform.position + transform.right * -width + transform.up * -height - bottomLeftBound;
        Vector3 camBottomRight = transform.position + transform.right * width + transform.up * -height - bottomLeftBound;

        float top = Vector3.Dot(upBoundAxis, camTopRight);
        float right = Vector3.Dot(rightBoundAxis, camTopRight);
        float bottom = Vector3.Dot(upBoundAxis, camBottomLeft);
        float left = Vector3.Dot(rightBoundAxis, camBottomLeft);

        if (left < 0f) transform.position += transform.right * -left;
        if (right > boundWidth) transform.position += transform.right * (boundWidth - right);

        if (bottom < 0f) transform.position += transform.up * -bottom;
        if (top > boundHeight) transform.position += transform.up * (boundHeight - top);

        //Debug.LogFormat("T: {0}; R: {1}; B: {2}; L: {3}", top, right, bottom, left);
    }

    public void ZoomControl()
    {
        Vector3 currentTouchPos = camera.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButton(0) && Input.touchCount < 2)
        {
            Vector3 diff = lastTouchPos - currentTouchPos;
            transform.position += diff;
        }

        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            Zoom(difference * 0.01f);
            return;
        }

        camera.orthographicSize -= camera.orthographicSize * Input.mouseScrollDelta.y / 10f;
        lastTouchPos = camera.ScreenToWorldPoint(Input.mousePosition);
    }

    void Zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }
}
