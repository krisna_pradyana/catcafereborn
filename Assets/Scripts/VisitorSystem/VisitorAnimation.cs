﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorAnimation : MonoBehaviour
{
    public System.Action<Sprite> SetFood;

    public enum LastDirection { north, east, south, west }

    public Visitor visitor { get; private set; }
    public Customer customer { get; private set; }
    public VisitorPathfinding pathfinding { get; private set; }

    [SerializeField] private Animator npcAnimator;
    [SerializeField] private SpriteRenderer npcSprite;

    public LastDirection lastDirection = LastDirection.south;
    public bool isFoodDisplayed { get; set; }

    private void Awake()
    {
        visitor = GetComponent<Visitor>();
        customer = GetComponent<Customer>();
        pathfinding = GetComponent<VisitorPathfinding>();

        //npcAnimator = transform.GetChild(0).GetComponent<Animator>();
        //npcSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();

    }

    private void OnEnable()
    {
        customer.OnWaitInQueue += OnCustomerQueue;
        customer.OnLeaveCafe += OnCustomerLeave;
        customer.OnEating += OnCustomerEating;
        visitor.OnSpawned += SetAnimatorController;        
    }

    private void OnDisable()
    {
        customer.OnWaitInQueue -= OnCustomerQueue;
        customer.OnLeaveCafe -= OnCustomerLeave;
        customer.OnEating -= OnCustomerEating;
        visitor.OnSpawned -= SetAnimatorController;
    }

    private void Start()
    {

    }

    private void Update()
    {
        SetAnimation();
    }

    private void SetAnimation()
    {
        Vector3 vel = pathfinding.velocity;
        npcAnimator.SetFloat("Xaxis", vel.x);
        npcAnimator.SetFloat("Zaxis", vel.z);

        if (Mathf.Abs(vel.z) > 0 || Mathf.Abs(vel.x) > 0)
        {
            //Debug.Log("Ai is moving");
            npcAnimator.SetBool("isIdle", false);
        }
        else
        {
            //Debug.Log("Ai is idling");
            npcAnimator.SetBool("isIdle", true);
        }

        if (Mathf.Abs(vel.z) > Mathf.Abs(vel.x))
            npcSprite.flipX = true;
        else
            npcSprite.flipX = false;

        if (!npcAnimator.GetBool("isEating"))
            SetLastDirection(vel.x, vel.z);
    }

    private void SetLastDirection(float x, float z)
    {
        if (x == 0 && z == 0) return;

        if (Mathf.Abs(z) > Mathf.Abs(x))
        {
            if (z > 0) lastDirection = LastDirection.north;
            else lastDirection = LastDirection.south;
        }
        else
        {
            if (x > 0) lastDirection = LastDirection.east;
            else lastDirection = LastDirection.west;
        }

        if (customer.state != Customer.State.Waiting)
            npcAnimator.SetBool("isPositive", (lastDirection == LastDirection.north || lastDirection == LastDirection.east));
        else
            npcAnimator.SetBool("isPositive", true);
    }

    /// <summary>
    /// Animator Related Function, Set Whether the animation bool if Holding Tray is true
    /// </summary>
    /// <param name="condition"></param>
    private void SetHoldingTray(bool condition)
    {
        Debug.Log("Setting Tray");
        npcAnimator.SetBool("holding_tray", condition);
    }

    /// <summary>
    ///  Animator Related Function, Set Whether the animation bool if Eating is true
    /// </summary>
    /// <param name="condition"></param>
    private void SetEating(bool condition)
    {
        npcAnimator.SetBool("isEating", condition);
    }

    /// <summary>
    ///  Animator Related Function, Set Whether the animation bool Positive is true
    /// </summary>
    /// <param name="condition"></param>
    private void SetPositive(bool condition)
    {
        npcAnimator.SetBool("isPositive", condition);
    }

    private void SetAnimatorController(Visitor visitor)
    {
        npcAnimator.runtimeAnimatorController = visitor.visitorType.animator;
    }

    private void OnCustomerQueue(Customer customer)
    {
        SetHoldingTray(true);
    }

    private void OnCustomerEating(Customer customer)
    {
        Debug.LogFormat(gameObject, "Setting is positive bool to {0}", customer.chair.isHeadPositive);

        SetPositive(customer.chair.isHeadPositive);
        SetHoldingTray(false);
        SetEating(true);
    }

    private void OnCustomerLeave(Customer customer)
    {
        SetHoldingTray(false);
        SetEating(false);
    }

}
