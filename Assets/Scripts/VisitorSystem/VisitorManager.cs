﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisitorManager : MonoBehaviour
{
    public static VisitorManager instance;

    [Header("Visitor, Location and Customer Food Setup")]
    public GameObject visitorPrefabs;
    public VisitorType[] visitorTypes;
    public FoodType[] foodTypes;
    public Transform[] visitorSpawnPoints;

    [Header("Visitor Avoidance")]
    [Range(0f, 10f)] public float avoidanceRadius = 1f;
    [Range(0f, 5f)]  public float avoidanceWeight = 0.5f;
    [Range(0f, 5f)]  public float pathfindingWeight = 0.8f;

    [Header("Store Setup")]
    [Tooltip("Higher Number, is Higher number of Visitors")] [Range(0f, 1f)] public float cafePower; // Higher number, higher number of visitor

    [Header("Debug")]
    [SerializeField] private Slider visitorPower;

    public List<Visitor> spawnedVisitors { get; private set; }
    public Transform[] visitorExits { get { return visitorSpawnPoints; } }

    private void Awake()
    {
        #region Singleton
        if (instance != null)
        {
            Debug.LogErrorFormat(instance, "There cannot be more than 1 VisitorManager.");
            Destroy(this);
        }

        else instance = this;
        #endregion

        ObjectPooler.CreatePool(visitorPrefabs);
        spawnedVisitors = new List<Visitor>();
    }

    /*
    [ContextMenu("Test")]
    private void Test()
    {
        GameObject obj = Instantiate(visitorPrefabs, new Vector3(10, 0.1f, 10), Quaternion.identity);

        Visitor visitor = obj.GetComponent<Visitor>();
        visitor.OnSpawned += OnVisitorSpawned;
        visitor.OnDespawned += OnVisitorDespawned;
    }
    */

    private void Update()
    {
        ///Realtime Set customer intensity
        Visitor.cafePower = cafePower;

        ///Game Speed
        if (Input.GetKeyDown(KeyCode.V))
            Time.timeScale = 2.5f;

        if (Input.GetKeyUp(KeyCode.V))
            Time.timeScale = 1f;

        //Debug
        visitorPower.value = cafePower;

    }

    private void Start()
    {
        ///Randomize spawn points
        for (int i = 0; i < visitorSpawnPoints.Length; i++)
            StartCoroutine(SpawnCoroutine(i));
    }

    private IEnumerator SpawnCoroutine(int spawnIndex)
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 10f));

            GameObject obj = ObjectPooler.Spawn(visitorPrefabs, visitorSpawnPoints[spawnIndex].position, Quaternion.identity);

            Visitor visitor = obj.GetComponent<Visitor>();
            visitor.OnSpawned += OnVisitorSpawned;
            visitor.OnDespawned += OnVisitorDespawned;
        }
    }

    private void OnVisitorSpawned(Visitor visitor)
    {
        spawnedVisitors.Add(visitor);
        Debug.Log("Adding visitor");
    }

    private void OnVisitorDespawned(Visitor visitor)
    {
        Debug.Log("Removing visitor");
        spawnedVisitors.Remove(visitor);

        visitor.OnSpawned -= OnVisitorSpawned;
        visitor.OnDespawned -= OnVisitorDespawned;
    }
}
