﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VisitorPathfinding), typeof(Customer))]
public class Visitor : MonoBehaviour, IPoolable
{
    public event System.Action<Visitor> OnSpawned;
    public event System.Action<Visitor> OnDespawned;

    public VisitorType visitorType;

    public bool isCustomer { get; private set; }
    public Customer customer { get; private set; }
    public VisitorPathfinding pathfinding { get; private set; }
    public VisitorManager visitorManager { get; private set; }
    public static float cafePower { get; set; }
    

    private void Awake()
    {
        pathfinding = GetComponent<VisitorPathfinding>();
        customer = GetComponent<Customer>();
    }

    private void Start()
    {
        visitorManager = VisitorManager.instance;
    }

    void IPoolable.OnSpawned()
    {
        visitorType = visitorManager.visitorTypes[Random.Range(0, visitorManager.visitorTypes.Length)]; // Randomize the visitor types (Means type of sprite like "Agent" or "Kid")
        SetHeightToFoor(visitorType);

        isCustomer = Random.value + 0.1f < cafePower; // Randomize if the visitor is a customer or not
        pathfinding.InitGraphMask(isCustomer);

        if (isCustomer) customer.StartBehaviour();
        else pathfinding.LeaveGameArea();

        if (OnSpawned != null)
            OnSpawned(this);
    }

    void IPoolable.OnDespawned()
    {
        if (OnDespawned != null)
            OnDespawned(this);
    }

    private void Update()
    {
        if (pathfinding.leavingGameArea && pathfinding.HasArrivedAtDestination())
            ObjectPooler.Despawn(gameObject);
    }

    /// <summary>
    /// Some NPC Character has shorter sprite height, so they will look flying than stand on floor
    /// in order to fix that this function run on start NPC spawned
    /// </summary>
    private void SetHeightToFoor(VisitorType _visitorType)
    {
        transform.GetChild(0).transform.position = new Vector3 (transform.GetChild(0).transform.position.x , _visitorType.heightToFoor, transform.GetChild(0).transform.position.z);   
    }
}
