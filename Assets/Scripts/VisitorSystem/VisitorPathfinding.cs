﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VisitorAI))]
public class VisitorPathfinding : MonoBehaviour
{
    public enum GraphMaskAI
    {
        None = 0,
        Customer = 1 << 0,
        Pedestrian = 1 << 1
    }

    public VisitorAI ai { get; protected set; }
    public Seeker seeker { get; protected set; }
    public Vector3 velocity { get { return ai.velocity; } }

    public bool leavingGameArea { get; protected set; }
    public bool avoidanceEnabled { get { return ai.avoidanceEnabled; } }

    private VisitorManager visitorManager;

    public void SetAvoidance(bool enabled) { ai.SetAvoidance(enabled); }

    private void Awake()
    {
        seeker = GetComponent<Seeker>();
        ai = GetComponent<VisitorAI>();
    }

    private void Start()
    {
        visitorManager = VisitorManager.instance;
    }

    public void InitGraphMask(bool isCustomer)
    {
        if (isCustomer)
            seeker.graphMask = (int)GraphMaskAI.Customer;
        else
            seeker.graphMask = (int)GraphMaskAI.Pedestrian;
    }

    public void ResetData()
    {
        leavingGameArea = false;
    }

    public void ForceSearchPath()
    {
        ai.SearchPath();
    }

    public bool HasArrivedAtDestination()
    {
        return ai.remainingDistance < ai.endReachedDistance;
    }

    public void SetDestination(Vector3 destination)
    {
        leavingGameArea = false;
        ai.destination = destination;
    }

    public void LeaveGameArea()
    {
        if (visitorManager == null)
            visitorManager = VisitorManager.instance;

        Transform[] visitorExits = visitorManager.visitorExits;
        int exitIndex = Random.Range(0, visitorExits.Length);

        if (transform.position == visitorExits[exitIndex].position)
            exitIndex = (exitIndex + 1) % visitorExits.Length;

        ai.destination = visitorExits[exitIndex].position;
        ai.SearchPath();

        leavingGameArea = true;
    }
}
