﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Visitor Type", menuName = "Visitor/Visitor Type")]
public class VisitorType : ScriptableObject
{
    public string name = "Default";
    public RuntimeAnimatorController animator;
    public float heightToFoor = 0;
    public float foodOffset = 0;
}
