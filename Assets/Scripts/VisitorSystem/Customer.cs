﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customer : MonoBehaviour
{
    public event System.Action<Customer> OnWaitInQueue;
    public event System.Action<Customer> OnLeaveCafe;
    public event System.Action<Customer> OnEating;

    public enum State
    {
        /// <summary> The customer just spawned an go to the shop. </summary>
        Going,
        /// <summary> The customer is waiting in the queue. </summary>
        Waiting,
        /// <summary> The customer had the food an going to a table/chair. </summary>
        FindChair,
        /// <summary> The customer is eating their food. </summary>
        Eating,
        /// <summary> The customer is done and leaving the shop. </summary>
        Leaving
    }

    public IEnumerator currentCoroutine;

    [HideInInspector] public State state;
    [HideInInspector] public Chair chair;
    [HideInInspector] public int queueNumber;

    private CafeManager cafe;
    private Visitor visitor;
    private VisitorPathfinding pathfinding;
    private new Collider collider;

    private VisitorManager visitorManager;
    private FoodType foodType ;

    private void Awake()
    {
        pathfinding = GetComponent<VisitorPathfinding>();
        collider = GetComponent<Collider>();
    }

    private void Start()
    {
        cafe = CafeManager.instance;
        visitorManager = VisitorManager.instance;
    }

    public void StartBehaviour()
    {
        if (cafe == null)
            cafe = CafeManager.instance;

        if (pathfinding == null)
            pathfinding = GetComponent<VisitorPathfinding>();

        StartCoroutine(GetInQueue());
    }

    private IEnumerator GetInQueue()
    {
        state = State.Going;

        if (cafe.isLongQueue)
        {
            Debug.LogFormat(gameObject, "Customer can't get in queue or the table is full. Leaving");
            pathfinding.LeaveGameArea();
            state = State.Leaving;
            yield break;
        }
        
        // The customer is going inside the cafe, but not yet get in queue
        pathfinding.SetDestination(cafe.GetQueuePosition());
        pathfinding.ForceSearchPath();

        cafe.OnQueueUpdate += pathfinding.ForceSearchPath;

        yield return null;

        // Wait until the customer has arrived in queue or the cafe is full
        while (!pathfinding.HasArrivedAtDestination() && !cafe.isLongQueue)
        {
            yield return null;
            pathfinding.SetDestination(cafe.GetQueuePosition());
        }
        
        cafe.OnQueueUpdate -= pathfinding.ForceSearchPath;

        if (cafe.isLongQueue)
        {
            Debug.LogFormat(gameObject, "Customer can't get in queue or the table is full. Leaving");
            pathfinding.LeaveGameArea();
            state = State.Leaving;
            yield break;
        }

        // If the customer has arrived in the queue, get the queue number
        queueNumber = cafe.GetQueueNumber(this);
        
        // The customer doesn't get a queue
        if (queueNumber == -1)
        {
            Debug.LogFormat(gameObject, "Customer can't get in queue. Leaving");
            pathfinding.LeaveGameArea();
            state = State.Leaving;
            yield break;
        }
        state = State.Waiting;

        // The customer is in queue, go to the next step
        foodType = visitorManager.foodTypes[Random.Range(0, visitorManager.foodTypes.Length)];
        StartCoroutine(WaitInQueue());
    }

    private IEnumerator WaitInQueue()
    {
        pathfinding.SetAvoidance(false);


        if (collider != null) collider.enabled = false;

        if (OnWaitInQueue != null)
            OnWaitInQueue(this);    //Setting Tray Value for visitor

        // While it's not the customers turn
        while (queueNumber >= 0)
        {
            int currentQueueNumber = queueNumber;
            Vector3 queuePos = cafe.GetQueuePosition(queueNumber);
            pathfinding.SetDestination(queuePos);
            pathfinding.ForceSearchPath();

            //transform.position = new Vector3(transform.position.x, transform.position.y, queuePos.z);
            // Wait until the next serving, then move forward
            yield return new WaitUntil(() => currentQueueNumber != queueNumber);
        }

        // The customer has the food, go to the next step
        // Randomize Customer Food
        Debug.Log("Enabling FoodSprite");

        StartCoroutine(FindChair());
    }

    private IEnumerator FindChair()
    {
        if (!cafe.isFull)
        {
            pathfinding.SetAvoidance(true);
            state = State.FindChair;
            // Get an empty chair in the cafe
            chair = cafe.GetChair(this);

            // Go to the picked chair
            pathfinding.SetDestination(chair.transform.position);
            pathfinding.ForceSearchPath();
            yield return null;

            while (!pathfinding.HasArrivedAtDestination())
                yield return null;

            // The customer has sat on the chair, go to the next step
            StartCoroutine(EatFood());
        }
        else
        {
            Debug.LogFormat(gameObject, "Customer can't a chair. Leaving");
            pathfinding.LeaveGameArea();
            state = State.Leaving;
            yield break;
        }
    }

    private IEnumerator EatFood()
    {
        float eatTime = Random.Range(10, 15);

        pathfinding.SetAvoidance(false);
        chair.FoodOnTable(foodType.foodSprite); // this is food sprite on table
        state = State.Eating;
        transform.position = chair.transform.position;

        if (OnEating != null)
            OnEating(this); //this will trigger eating animation

        yield return new WaitForSeconds(eatTime); ///Eat Time for Visitor while sitting on Chair

        // The customer has finished eating the food and now leaving the cafe
        state = State.Leaving;

        chair.LeaveChair();
        pathfinding.LeaveGameArea();
        pathfinding.ForceSearchPath();

        if (OnLeaveCafe != null)
            OnLeaveCafe(this);

        if (collider != null) collider.enabled = true;
        pathfinding.SetAvoidance(true);
    }

}
