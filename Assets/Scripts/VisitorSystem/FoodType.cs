﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Food Type", menuName = "Food/Food Type")]
public class FoodType : ScriptableObject
{
    public string name = "Default";
    public Sprite foodSprite;
    public Sprite coffeeOnly;
}
