﻿using Pathfinding;
using Pathfinding.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorAI : AIPath
{
    private VisitorPathfinding visitor;
    private VisitorManager visitorManager;

    private float avoidanceRadiusSq { get { return visitorManager.avoidanceRadius * visitorManager.avoidanceRadius; } }
    private float pathfindingWeight { get { return visitorManager.pathfindingWeight; } }
    private float avoidanceWeight { get { return visitorManager.avoidanceWeight; } }

    private Quaternion rotator = Quaternion.Euler(0f, 0f, 10f);
    private Quaternion rotatorN = Quaternion.Euler(0f, 0f, -10f);

    public bool avoidanceEnabled;
    public void SetAvoidance(bool enabled) { avoidanceEnabled = enabled; }
    
    protected override void Start()
    {
        visitorManager = VisitorManager.instance;
        visitor = GetComponent<VisitorPathfinding>();

        //avoidanceRadiusSq = visitorManager.avoidanceRadius * visitorManager.avoidanceRadius;
        //avoidanceWeight = visitorManager.avoidanceWeight;

        avoidanceEnabled = true;

        base.Start();
    }

    private Vector3 GetAvoidanceVelocity()
    {
        List<Visitor> spawnedVisitors = new List<Visitor>(visitorManager.spawnedVisitors);
        Vector3 avoidanceVelocity = Vector3.zero;
        float count = 0;

        for (int i = 0; i < spawnedVisitors.Count; i++)
        {
            Visitor checkedVisitor = spawnedVisitors[i];

            if (checkedVisitor == visitor || !checkedVisitor.pathfinding.avoidanceEnabled)
                continue;

            Transform checkedTransform = checkedVisitor.transform;
            Vector3 difference = transform.position - checkedTransform.position;
            float sqrDistance = difference.sqrMagnitude;

            if (sqrDistance > 0 && sqrDistance < avoidanceRadiusSq)
                avoidanceVelocity += difference.normalized / sqrDistance;

            count++;
        }
        
        if (count != 0) avoidanceVelocity /= count;

        return avoidanceVelocity;
    }

    protected override void OnDrawGizmos()
    {
        //Gizmos.color = Color.cyan;
        //Gizmos.DrawWireSphere(transform.position, visitorManager.avoidanceRadius);
            
        //base.OnDrawGizmos();
    }

    protected override void MovementUpdateInternal(float deltaTime, out Vector3 nextPosition, out Quaternion nextRotation)
    {
        //base.MovementUpdateInternal(deltaTime, out nextPosition, out nextRotation);
        //return;

        #region Base MovementUpdate
        float currentAcceleration = maxAcceleration;

        // If negative, calculate the acceleration from the max speed
        if (currentAcceleration < 0) currentAcceleration *= -maxSpeed;

        if (updatePosition)
        {
            // Get our current position. We read from transform.position as few times as possible as it is relatively slow
            // (at least compared to a local variable)
            simulatedPosition = tr.position;
        }
        if (updateRotation) simulatedRotation = tr.rotation;

        var currentPosition = simulatedPosition;

        // Update which point we are moving towards
        interpolator.MoveToCircleIntersection2D(currentPosition, pickNextWaypointDist, movementPlane);
        var dir = movementPlane.ToPlane(steeringTarget - currentPosition);

        // Calculate the distance to the end of the path
        float distanceToEnd = dir.magnitude + Mathf.Max(0, interpolator.remainingDistance);

        // Check if we have reached the target
        var prevTargetReached = reachedEndOfPath;
        reachedEndOfPath = distanceToEnd <= endReachedDistance && interpolator.valid;
        if (!prevTargetReached && reachedEndOfPath) OnTargetReached();
        float slowdown;

        // Normalized direction of where the agent is looking
        var forwards = movementPlane.ToPlane(simulatedRotation * (orientation == OrientationMode.YAxisForward ? Vector3.up : Vector3.forward));

        // Check if we have a valid path to follow and some other script has not stopped the character
        if (interpolator.valid && !isStopped)
        {
            // How fast to move depending on the distance to the destination.
            // Move slower as the character gets closer to the destination.
            // This is always a value between 0 and 1.
            slowdown = distanceToEnd < slowdownDistance ? Mathf.Sqrt(distanceToEnd / slowdownDistance) : 1;

            if (reachedEndOfPath && whenCloseToDestination == CloseToDestinationMode.Stop)
            {
                // Slow down as quickly as possible
                velocity2D -= Vector2.ClampMagnitude(velocity2D, currentAcceleration * deltaTime);
            }
            else
            {
                velocity2D += MovementUtilities.CalculateAccelerationToReachPoint(dir, dir.normalized * maxSpeed, velocity2D, currentAcceleration, rotationSpeed, maxSpeed, forwards) * deltaTime;
            }
        }
        else
        {
            slowdown = 1;
            // Slow down as quickly as possible
            velocity2D -= Vector2.ClampMagnitude(velocity2D, currentAcceleration * deltaTime);
        }

        velocity2D = MovementUtilities.ClampVelocity(velocity2D, maxSpeed, slowdown, slowWhenNotFacingTarget && enableRotation, forwards);
        
        ApplyGravity(deltaTime);
        #endregion

        if (avoidanceEnabled)
        {
            //Debug.DrawLine(transform.position + Vector3.up * 1, transform.position + new Vector3(velocity2D.x, 0, velocity2D.y) + Vector3.up * 1, Color.green);

            Vector2 avoidanceVelocity = GetAvoidanceVelocity() * avoidanceWeight;

            if (avoidanceVelocity.sqrMagnitude != 0)
            {
                float a = Vector2.SignedAngle(velocity2D, dir);

                if (Vector2.Dot(avoidanceVelocity, dir) < 0)
                {
                    if (a >= 0f)
                    {
                        avoidanceVelocity = rotator * avoidanceVelocity;
                        velocity2D = rotator * velocity2D;
                    }
                    else
                    {
                        avoidanceVelocity = rotatorN * avoidanceVelocity;
                        velocity2D = rotatorN * velocity2D;
                    }
                }

                velocity2D += avoidanceVelocity;

                Vector2 n = velocity2D.normalized;
                Debug.DrawLine(transform.position + Vector3.up * 1.5f, transform.position + new Vector3(n.x, 0f, n.y) * (a / 180) + Vector3.up * 1.5f, Color.white);
            }

            Debug.DrawLine(transform.position + Vector3.up * 1, transform.position + new Vector3(velocity2D.x, 0, velocity2D.y) + Vector3.up * 1, Color.magenta);
            Debug.DrawLine(transform.position + Vector3.up * 1, transform.position + new Vector3(avoidanceVelocity.x, 0, avoidanceVelocity.y) + Vector3.up * 1, Color.red);
        }

        #region Base Final Calculation
        // Set how much the agent wants to move during this frame
        var delta2D = lastDeltaPosition = CalculateDeltaToMoveThisFrame(movementPlane.ToPlane(currentPosition), distanceToEnd, deltaTime);
        nextPosition = currentPosition + movementPlane.ToWorld(delta2D, verticalVelocity * lastDeltaTime);
        CalculateNextRotation(slowdown, out nextRotation);
        #endregion
    }
}
