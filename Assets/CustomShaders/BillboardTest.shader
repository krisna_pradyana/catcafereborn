﻿Shader "CustomShader/Billboard"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
				"DisableBatching" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			CGPROGRAM
			#pragma surface surf Lambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing  addshadow
			#pragma multi_compile_local _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnitySprites.cginc"

			struct Input
			{
				float2 uv_MainTex;
				fixed4 color;
			};

			void vert(inout appdata_full v, out Input o)
			{
				v.vertex = UnityFlipSprite(v.vertex, _Flip);

				#if defined(PIXELSNAP_ON)
				v.vertex = UnityPixelSnap(v.vertex);
				#endif

				UNITY_INITIALIZE_OUTPUT(Input, o);

				// apply object scale
				v.vertex.xy *= float2(length(unity_ObjectToWorld._m00_m10_m20), length(unity_ObjectToWorld._m01_m11_m21));

				// get the camera basis vectors
				float3 forward = -normalize(UNITY_MATRIX_V._m20_m21_m22);
				float3 up = normalize(UNITY_MATRIX_V._m10_m11_m12);
				float3 right = normalize(UNITY_MATRIX_V._m00_m01_m02);

				// rotate to face camera
				float4x4 rotationMatrix = float4x4(right, 0,
					up, 0,
					forward, 0,
					0, 0, 0, 1);
				v.vertex = mul(v.vertex, rotationMatrix);
				v.normal = mul(v.normal, rotationMatrix);

				// undo object to world transform surface shader will apply
				v.vertex.xyz = mul((float3x3)unity_WorldToObject, v.vertex.xyz);
				v.normal = mul(v.normal, (float3x3)unity_ObjectToWorld);

				o.color = v.color * _Color * _RendererColor;
			}

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = SampleSpriteTexture(IN.uv_MainTex) * IN.color;
				o.Albedo = c.rgb * c.a;
				o.Alpha = c.a;
			}
			ENDCG
		}

			Fallback "Transparent/VertexLit"
}

//Shader "Sprites/Billboard"
//{
//	Properties
//	{
//		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
//		_Color("Tint", Color) = (1,1,1,1)
//		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
//	}
//
//	SubShader
//	{
//		Tags
//		{
//			"Queue" = "Transparent"
//			"IgnoreProjector" = "True"
//			"RenderType" = "Transparent"
//			"PreviewType" = "Plane"
//			"CanUseSpriteAtlas" = "True"
//			"DisableBatching" = "True"
//		}
//
//		Cull Off
//		Lighting Off
//		ZWrite Off
//		Blend One OneMinusSrcAlpha
//
//		Pass
//		{
//		CGPROGRAM
//			#pragma vertex vert
//			#pragma fragment frag
//			#pragma multi_compile _ PIXELSNAP_ON
//			#include "UnityCG.cginc"
//
//			struct appdata_t : appdata_base
//			{
//				float4 vertex   : POSITION;
//				float4 color    : COLOR;
//			};
//
//			struct v2f
//			{
//				float4 vertex   : SV_POSITION;
//				fixed4 color : COLOR;
//				float2 texcoord  : TEXCOORD0;
//			};
//
//			fixed4 _Color;
//			sampler2D _MainTex;
//			sampler2D _AlphaTex;
//			float _AlphaSplitEnabled;
//			float4 _MainTex_ST;
//
//			v2f vert(appdata_t IN)
//			{
//				v2f OUT;
//
//				OUT.vertex = mul(UNITY_MATRIX_P,
//					mul(UNITY_MATRIX_MV, float4(0, 0, 0, 1)) + float4(IN.vertex.x, IN.vertex.y, 0, 0));
//
//				OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
//				OUT.color = IN.color * _Color;
//				 
//				#ifdef PIXELSNAP_ON
//				OUT.vertex = UnityPixelSnap(OUT.vertex);
//				#endif
//				
//				return OUT;
//			}
//
//
//			fixed4 SampleSpriteTexture(float2 uv)
//			{
//				fixed4 color = tex2D(_MainTex, uv);
//
//				#if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
//				if (_AlphaSplitEnabled)
//					color.a = tex2D(_AlphaTex, uv).r;
//				#endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED
//
//				return color;
//			}
//
//			fixed4 frag(v2f IN) : SV_Target
//			{
//				fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;
//				c.rgb *= c.a;
//				return c;
//			}
//		ENDCG
//		}
//	}
//}
//Shader "Custom/Billboard"
//{
//	Properties 
//	{
//		_MainTex("Texture", 2D) = "white" {}
//		_Color("Tint", Color) = (1,1,1,1)
//		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
//	}
//		SubShader
//	{
//		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
//
//		Blend SrcAlpha OneMinusSrcAlpha
//		Pass
//		{
//			CGPROGRAM
//			#pragma vertex vert
//			#pragma fragment frag      
//			#include "UnityCG.cginc"
//
//			struct v2f
//			{
//				float4 pos : SV_POSITION;
//				float2 uv : TEXCOORD0;
//			};
//
//			sampler2D _MainTex;
//			float4 _MainTex_ST;
//
//			v2f vert(appdata_base v)
//			{
//				v2f o;
//				o.pos = mul(UNITY_MATRIX_P,
//				mul(UNITY_MATRIX_MV, float4(0, 0, 0, 1)) + float4(v.vertex.x, v.vertex.y, 0, 0));
//				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
//				return o;
//			}
//
//			fixed4 frag(v2f i) : SV_Target
//			{
//				fixed4 col = tex2D(_MainTex, i.uv);
//				return col;
//			}
//			ENDCG
//		}
//	}
//}