﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DragonBones;

public class DragonBoneCollector : EditorWindow
{
    static Object[] collectionOfData;
    Object[] lastSavedData;
    int lengthField = 0;

    [MenuItem("DragonBoneAddon/DragonBoneCollectData")]
    static void ShowWindow()
    {
        EditorWindow.GetWindow<DragonBoneCollector>("Dragon Bone Collector");
    }

    private void OnGUI()
    {
        GUILayout.Label("Dragon Bone Collector", EditorStyles.boldLabel);
        lengthField = EditorGUILayout.IntField("Start Level in Number : ", lengthField);

        if(collectionOfData != null)
        {
            ShowArray(collectionOfData, typeof(UnityDragonBonesData));
        }

        if (GUILayout.Button("Set Lenght"))
        {
            collectionOfData = new Object[lengthField];
        }

        if(GUILayout.Button("Collect Data"))
        {
            for(int i =0; i< collectionOfData.Length; i++)
            {
                CollectData(collectionOfData[i] as UnityDragonBonesData);
            }
        }
    }

    public static void ShowArray(Object[] objects, System.Type type)
    {
        for (int i = 0; i < collectionOfData.Length; i++)
        {
            collectionOfData[i] = EditorGUILayout.ObjectField("Dragon Bone Asset File", objects[i], type, false);
        }
    }

    void CollectData(UnityDragonBonesData _bonesData)
    {
        string[] skeToFind = AssetDatabase.FindAssets(_bonesData.dataName + "_ske" + " t:TextAsset", null);
        foreach (string skePath in skeToFind)
        {
            _bonesData.dragonBonesJSON = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(skePath), typeof(TextAsset)) as TextAsset;
            Debug.Log(AssetDatabase.LoadAssetAtPath(skePath, typeof(TextAsset)) as TextAsset);
            AssetDatabase.Refresh();
        }

        string[] atlasToFind = AssetDatabase.FindAssets(_bonesData.dataName + "_tex" + " t:TextAsset", null);
        foreach (string atlsPath in atlasToFind)
        {
            _bonesData.textureAtlas[0].textureAtlasJSON = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(atlsPath), typeof(TextAsset)) as TextAsset;
            AssetDatabase.Refresh();
        }

        string[] texToFind = AssetDatabase.FindAssets(_bonesData.dataName + "_tex" + " t:Texture2D", null);
        foreach (string texPath in texToFind)
        {
            _bonesData.textureAtlas[0].texture = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(texPath), typeof(Texture2D)) as Texture2D;
            AssetDatabase.Refresh();
        }

        string[] matToFind = AssetDatabase.FindAssets(_bonesData.dataName + "_tex_Mat" + " t:Material", null);
        foreach (string matPath in matToFind)
        {
            _bonesData.textureAtlas[0].material = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(matPath), typeof(Material)) as Material;
            AssetDatabase.Refresh();
        }

        /*
        string[] uiMatToFind = AssetDatabase.FindAssets(bonesData.dataName + "_ske", null);
        foreach (string path in uiMatToFind)
        {
            Debug.Log(AssetDatabase.GUIDToAssetPath(path));
            bonesData.textureAtlas[0].textureAtlasJSON = (TextAsset)AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset));
        }
        */
    }
}
