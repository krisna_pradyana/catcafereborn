﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelNameGenerator : EditorWindow
{
    GameObject[] levelsInScene;
    List <GameObject> sortedLevels;
    int levelField = 1;

    [MenuItem("Window/LevelNameGenerator")]
    static void ShowWindow()
    {
        EditorWindow.GetWindow<LevelNameGenerator>("LevelGeneratorWindow");
    }

    private void OnGUI()
    {
        sortedLevels = new List<GameObject>();
        GUILayout.Label("Level Generator", EditorStyles.boldLabel);
        GUILayout.Label("Level Start");
        levelField = EditorGUILayout.IntField("Start Level in Number : ", levelField);

        Debug.Log(levelField);
        //GUILayout.Label("Level End");
        //EditorGUILayout.IntField(levelEnd);

        if (GUILayout.Button("Generate Level"))
        {
            FindAllLevelGameObjects(levelField);
        }

        if (GUILayout.Button("Reset"))
        {
            ResetArrays();
        }
    }

    public void FindAllLevelGameObjects(int _levelStart)
    {
        Debug.Log("Func work properly : " + _levelStart);
        levelsInScene = GameObject.FindGameObjectsWithTag("ChangedLevel");

        for (int i = 0; i < levelsInScene.Length + 1 ; i++)
        {
            for (int j = 0; j < levelsInScene.Length; j++)
            {
                if (levelsInScene[j].transform.GetSiblingIndex() == i)
                {
                    sortedLevels.Add(levelsInScene[j]);
                }
            }
        }

        for (int i = 0; i < sortedLevels.Count; i++)
        {
            sortedLevels[i].name = "Level " + (_levelStart + i).ToString();
        }
    }

    void ResetArrays()
    {
        levelsInScene = null;
        sortedLevels.Clear(); 
    }
}
